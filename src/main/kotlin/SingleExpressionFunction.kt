fun hello(name: String): Unit = println("Hello $name")

fun main() {
    hello("Ryan")
}