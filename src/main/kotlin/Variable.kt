const val APP = "Belajar Kotlin"

fun main() {
//    Immutable
    val age: Int = 22

//    Mutable
    var name: String = "Ryan"

    name = "Aprianto"

//    Nullable
    val address: String? = null

    print(APP)
}