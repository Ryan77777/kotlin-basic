fun main() {
    var counter: Int = 0

    val lambdaIncrement: ()-> Unit = {
        println("Lambda")
        counter++
    }

    val anonymousIncrement = fun(){
        println("Anonymous")
        counter++
    }

    fun functionIncrement(){
        println("Func")
        counter++
    }
}