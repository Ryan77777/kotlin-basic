fun String.hello(): String = "Hello $this"

fun main() {
    val name = "Ryan"

    println(name.hello())
}