fun main() {
//    For Array
    var array: Array<String> = arrayOf("Ryan", "Teguh")

    var total = 0
    for (name in array){
        println(name)
        total++
    }
    println(total)

//    For Grade
    for (number in 1..100){
        println(number)
    }

    for (number in 100 downTo 1 step 5)
    {
        println(number)
    }
}