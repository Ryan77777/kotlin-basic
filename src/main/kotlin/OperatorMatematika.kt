fun main() {
    var result = 20.0 / 3.0

//    Augmented Assignments
//    a = a + 10 => a += 10

//    Unary Operator
//    ++ => a = a + 1
//    ! => boolean

    var total: Int = 0

    val product1 = 1000
    total += product1
    val product2 = 2000
    total += product2
    val product3 = 3000
    total += product3

    println(total)
}