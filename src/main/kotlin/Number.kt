fun main() {
//    Integer
    val age: Byte = 22
    val height: Int  = 170
    val distance: Short = 200
    val balance: Long = 21000000L

//    Underscore in number
    val sample: Int = 2_000_000

//    Floating Point Number
    val value: Float = 90.87F
    val radius: Double = 87864.29478

//    Literals
    val decimalLiteral: Int = 100
    val hexadecimalLiteral: Int = 0xFF
    val binaryLiteral: Int = 0b0001

    var number: Int = 100

//    Conversion
    val byte: Byte = number.toByte()
    val short: Short = number.toShort()
    val int: Int = number.toInt()
    val long: Long = number.toLong()
    val float: Float = number.toFloat()
    val double: Double = number.toDouble()


    print(age)
}