fun gretting(firstName: String, lastName: String?){
    if (lastName == null){
        println("Hello $firstName")
    } else {
        println("Hello $firstName $lastName")
    }
}
fun main() {
    gretting("Ryan", "A")
}