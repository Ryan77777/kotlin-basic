fun main() {
    fun hello(value: String, transformer: (String) -> String): String {
        return "Hello ${transformer(value)}"
    }

    val upTransformer  = { value: String -> value.uppercase()}
    val lowTransformer  = { value: String -> value.lowercase()}

    println(hello("Ryan", upTransformer))
    println(hello("Ryan", lowTransformer))

    val result = hello("Ryan") {value: String -> String
        value.uppercase()
    }

    println(result)
}