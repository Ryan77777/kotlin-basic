fun main() {
    fun sayHello(name: String = ""): String{
//        return if (name == ""){
//            "Hello bro"
//        } else {
//            "Hello $name"
//        }

        return when(name){
            "" -> "Hello bro"
            else -> "Hello $name"
        }
    }

    println(sayHello())
}