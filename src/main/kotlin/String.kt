fun main() {
    var firstName: String = "Ryan"
    var lastName: String = "Aprianto"
    var address: String = """
        |Panjalin Kidul,
        |Majalengka
    """.trimMargin()

    var fullName: String = firstName + " " + lastName

//    String template ($)
    fullName = "$firstName $lastName"

    print(fullName)
}