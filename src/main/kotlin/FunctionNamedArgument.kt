fun hi(firstName:String, lastName: String){
    println("Hello $firstName $lastName")
}
fun main() {
    hi(
        lastName = "Aprianto",
        firstName = "Ryan"
    )
}