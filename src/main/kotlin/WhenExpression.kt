fun main() {
    var grade = 'A'

//    Single Option checker
    when(grade){
        'A' -> {
            println("Bagus")
        }
        'B' -> println("Cukup Bagus")
        'C' -> println("Cukup")
        else -> println("Anda tidak lulus")
    }

//    Multiple Option checker
    when(grade){
        'A', 'B', 'C' -> {
            println("Lulus")
        }
        else -> println("Anda tidak lulus")
    }

    //    When Expression IN
    val passValues: Array<Char> = arrayOf('A', 'B')
    when(grade) {
        in passValues -> println("Lulus")
        !in passValues -> println("Anda tidak lulus")
    }

    //    When Expression Is
    val name = "Ryan"
    when(name) {
        is String ->println("Betul")
        !is String -> println("Salah")
    }

    val example = 90
    when {
        example > 90 -> println("Good")
        example < 90 -> println("Bad")
        else -> println("Something went wrong")

    }
}