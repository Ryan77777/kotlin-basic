fun toUpper(value: String): String = value.uppercase()

fun main() {
    val convertToLowercase: (String) -> String = {
        it.lowercase()
    }

    val convertToUppercase: (String) -> String = {firstName: String ->
        firstName.uppercase()
    }


    var name = convertToLowercase("Ryan")
    var cn = convertToUppercase("Ryan")

    val lambdaName: (String) -> String = ::toUpper

    val ex = lambdaName("HAGHJdjhfjd")

    println(name)
    println(cn)
    println(ex)
}